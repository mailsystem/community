<?php

namespace MailSystem\Bundle\DeliveryBundle\Migrations\schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * Class MailSystemDeliveryBundle
 * @package MailSystem\Bundle\DeliveryBundle\Migrations\Schema\v1_0
 */
class MailSystemDeliveryBundle implements Migration
{
    /**
     * @inheritdoc
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        if(!$schema->hasTable('ms_mailer_type')){
            $typeTable = $schema->createTable('ms_mailer_type');
            $typeTable->addColumn('id', 'integer', ['autoincrement' => true]);
            $typeTable->addColumn('code', 'string', ['notnull' => true]);
            $typeTable->addColumn('name', 'string', ['notnull' => true]);
            $typeTable->setPrimaryKey(['id']);
        }

        if(!$schema->hasTable('ms_mailer_version')){
            $versionTable = $schema->createTable('ms_mailer_version');
            $versionTable->addColumn('id', 'integer', ['autoincrement' => true]);
            $versionTable->addColumn('type_id', 'integer', ['notnull' => true]);
            $versionTable->addColumn('tag', 'string', ['notnull' => true]);
            $versionTable->addColumn('service_mailer', 'string', ['notnull' => true]);
            $versionTable->addColumn('extra', 'text', ['notnull' => false]);
            $versionTable->setPrimaryKey(['id']);

            $versionTable->addForeignKeyConstraint(
                $schema->getTable('ms_mailer_type'),
                ['type_id'],
                ['id'],
                []
            );
        }
    }
}
