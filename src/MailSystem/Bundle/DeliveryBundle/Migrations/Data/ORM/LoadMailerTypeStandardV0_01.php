<?php

namespace MailSystem\Bundle\DeliveryBundle\Migrations\Data\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use MailSystem\Bundle\DeliveryBundle\Entity\Mailer\Type;
use MailSystem\Bundle\DeliveryBundle\Entity\Mailer\Version;

class LoadMailerTypeStandardV0_01 extends AbstractFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $type = new Type();
        $type->setCode('ms_standard');
        $type->setName('Ms Standard');

        $version = new Version();
        $version->setType($type);
        $version->setTag('0.0.1');
        $version->setServiceMailer('ms.delivery.type.ms.standard.v0.0.1.mailer');

        $version->setExtra([
            'type' => 'standard',
            'usage' => 'all'
        ]);

        $manager->persist($type);
        $manager->persist($version);
        $manager->flush();
    }
}
