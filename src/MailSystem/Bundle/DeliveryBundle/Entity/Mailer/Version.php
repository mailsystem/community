<?php

namespace MailSystem\Bundle\DeliveryBundle\Entity\Mailer;

use Doctrine\ORM\Mapping as ORM;
use Oro\Bundle\DataAuditBundle\Metadata\Annotation as Oro;

/**
 * Mailer\Version
 *
 * @ORM\Table(name="ms_mailer_version")
 * @ORM\Entity(repositoryClass="MailSystem\Bundle\DeliveryBundle\Entity\Mailer\Repository\VersionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Version
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=128)
     */
    protected $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="service_mailer", type="string", length=255)
     */
    protected $serviceMailer;

    /**
     * @var Type
     *
     * @ORM\ManyToOne(targetEntity="MailSystem\Bundle\DeliveryBundle\Entity\Mailer\Type")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $type;

    /**
     * @var array
     *
     * @ORM\Column(name="extra", type="array")
     */
    protected $extra = [];

    /**
     * __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->tag;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Type $type
     * @return $this
     */
    public function setType(Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return string
     */
    public function getServiceMailer()
    {
        return $this->serviceMailer;
    }

    /**
     * @param string $serviceMailer
     * @return $this
     */
    public function setServiceMailer($serviceMailer)
    {
        $this->serviceMailer = $serviceMailer;

        return $this;
    }

    /**
     * @param $extra
     */
    public function setExtra(array $extra)
    {
        $this->extra = $extra;
    }

    /**
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }
}
