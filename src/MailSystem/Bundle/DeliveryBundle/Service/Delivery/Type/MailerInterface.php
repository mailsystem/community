<?php
/** {license_text} */

namespace MailSystem\Bundle\DeliveryBundle\Service\Delivery\Type;

use MailSystem\Bundle\LetterBundle\Entity\Letter;
use OroCRM\Bundle\ContactBundle\Entity\Group;

interface MailerInterface
{
    const MAILER_STATE_PENDING  = 'pending';
    const MAILER_STATE_RUNNING  = 'running';
    const MAILER_STATE_FINISHED = 'finished';
    const MAILER_STATE_FAILED   = 'failed';
    const MAILER_STATE_UNKNOWN  = 'unknown';

    public function send(Letter $letter, Group $group);
}
