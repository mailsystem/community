<?php
/** {license_text} */

namespace MailSystem\Bundle\DeliveryBundle\Service\Delivery\Type\Ms\Standard\v0_0_1;

use MailSystem\Bundle\DeliveryBundle\Service\Delivery\Type\MailerInterface;
use MailSystem\Bundle\LetterBundle\Entity\Letter;
use OroCRM\Bundle\ContactBundle\Entity\Group;
use Psr\Log\LoggerInterface;

/**
 * Class Mailer
 * @package MailSystem\Bundle\DeliveryBundle\Service\Delivery\Type\Ms\Standard\v0_0_1
 */
class Mailer implements MailerInterface
{
    /** @var  LoggerInterface */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

    }

    public function send(Letter $letter, Group $group)
    {
        $this->logger->info('Mailer v0.0.1 : send');
        // TODO: Implement send() method.
    }
}
