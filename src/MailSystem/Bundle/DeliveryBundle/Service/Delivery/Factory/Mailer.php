<?php

namespace MailSystem\Bundle\DeliveryBundle\Service\Delivery\Factory;

use MailSystem\Bundle\DeliveryBundle\Entity\Mailer\Version;
use MailSystem\Bundle\DeliveryBundle\Service\Delivery\Type\MailerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Mailer
{
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Retrieve version related mailer service
     *
     * @param Version $version
     * @return MailerInterface
     */
    public function getService(Version $version)
    {
        return $this->container->get($version->getServiceMailer());
    }
}
