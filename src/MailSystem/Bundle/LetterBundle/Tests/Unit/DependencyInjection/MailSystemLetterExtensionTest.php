<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Tests\Unit\DependencyInject;

use Oro\Bundle\TestFrameworkBundle\Test\DependencyInjection\ExtensionTestCase;

use MailSystem\Bundle\LetterBundle\DependencyInjection\MailSystemLetterExtension;

class MailSystemLetterExtensionTest extends ExtensionTestCase
{
    public function testLoad()
    {
        $this->loadExtension(new MailSystemLetterExtension());

        $expectedParameters = [
            'ms_letter.letter.form.type.class',
            'ms_letter.letter.form.handler.class',
            'ms_letter.letter_template.form.type.class',
            'ms_letter.letter_template.form.handler.class',
        ];
        $this->assertParametersLoaded($expectedParameters);
    }
}
