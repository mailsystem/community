<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Tests\Unit\Controller;

class LetterControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testIndexAction()
    {
        $view = "MailSystemLetterBundle:Letter:index.html.twig";

        $response = $this->getMock("Symfony\Component\HttpFoundation\Response");

        $template = $this->getMock('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface');
        $template
            ->expects($this->any())
            ->method("renderResponse")
            ->with($view, [], null)
            ->will($this->returnValue($response));
    }
}
