<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Tests\Unit\Entity;

use MailSystem\Bundle\LetterBundle\Entity\LetterTemplate;

/**
 * Class LetterTest
 * @package MailSystem\Bundle\LetterBundle\Tests\Unit\Entity
 */
class LetterTemplateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getSetDataProvider
     */
    public function testGetSet($property, $value, $expected)
    {
        $obj = new LetterTemplate();
        call_user_func_array([$obj, 'set' . ucfirst($property)], [$value]);
        $this->assertEquals($expected, call_user_func_array([$obj, 'get' . ucfirst($property)], []));
    }

    public function getSetDataProvider()
    {
        $now          = new \DateTime('now');
        $organization = $this->getMockBuilder('Oro\Bundle\OrganizationBundle\Entity\Organization')
            ->disableOriginalConstructor()
            ->getMock();
        $owner        = $this->getMockBuilder('Oro\Bundle\OrganizationBundle\Entity\BusinessUnit')
            ->disableOriginalConstructor()
            ->getMock();
        return [
            'organization' => ['organization', $organization, $organization],
            'owner'        => ['owner', $owner, $owner],
            'name'         => ['name', 'name', 'name'],
            'body'         => ['body', 'body', 'body'],
            'createdAt'    => ['createdAt', $now, $now],
            'updatedAt'    => ['updatedAt', $now, $now],
        ];
    }

    public function testBeforeSave()
    {
        $obj = new LetterTemplate();
        $this->assertNull($obj->getCreatedAt());
        $this->assertNull($obj->getUpdatedAt());
        $obj->prePersist();
        $this->assertInstanceOf('\DateTime', $obj->getCreatedAt());
        $this->assertNull($obj->getUpdatedAt());
    }

    public function testBeforeUpdate()
    {
        $obj = new LetterTemplate();
        $this->assertNull($obj->getCreatedAt());
        $this->assertNull($obj->getUpdatedAt());
        $obj->preUpdate();
        $this->assertInstanceOf('\DateTime', $obj->getUpdatedAt());
        $this->assertNull($obj->getCreatedAt());
    }
}
