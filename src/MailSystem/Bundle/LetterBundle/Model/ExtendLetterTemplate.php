<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Model;

class ExtendLetterTemplate
{
    /**
     * Constructor
     *
     * The real implementation of this method is auto generated.
     *
     * IMPORTANT: If the derived class has own constructor it must call parent constructor.
     */
    public function __construct()
    {

    }
}
