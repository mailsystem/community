<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use MailSystem\Bundle\LetterBundle\Entity\LetterTemplate;

/**
 * Class LetterTemplateController
 * @package MailSystem\Bundle\LetterBundle\Controller
 */
class LetterTemplateController extends Controller
{
    /**
     * @Route("/view/{id}", name="ms_letter_template_view", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="ms_letter_template_view",
     *      type="entity",
     *      permission="VIEW",
     *      class="MailSystemLetterBundle:LetterTemplate"
     * )
     * @param LetterTemplate $entity
     * @return array
     */
    public function viewAction(LetterTemplate $entity)
    {
        return [
            'entity' => $entity,
        ];
    }

    /**
     * @Route(name="ms_letter_template_index")
     * @Template
     * @AclAncestor("ms_letter_template_view")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/update/{id}", name="ms_letter_template_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="ms_letter_template_update",
     *      type="entity",
     *      permission="EDIT",
     *      class="MailSystemLetterBundle:LetterTemplate"
     * )
     * @param LetterTemplate $entity
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(LetterTemplate $entity)
    {
        return $this->update($entity);
    }

    /**
     * @Route("/create", name="ms_letter_template_create")
     * @Template("MailSystemLetterBundle:LetterTemplate:update.html.twig")
     * @Acl(
     *      id="ms_letter_template_create",
     *      type="entity",
     *      permission="CREATE",
     *      class="MailSystemLetterBundle:LetterTemplate"
     * )
     */
    public function createAction()
    {
        return $this->update(new LetterTemplate());
    }

    /**
     * @Route("/delete/{id}", name="ms_letter_template_delete", requirements={"id"="\d+"})
     * @Acl(
     *      id="ms_letter_template_delete",
     *      type="entity",
     *      permission="DELETE",
     *      class="MailSystemLetterBundle:LetterTemplate"
     * )
     * @param LetterTemplate $entity
     * @return JsonResponse
     */
    public function deleteAction(LetterTemplate $entity)
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($entity);
        $em->flush();
        return new JsonResponse('', Codes::HTTP_OK);
    }

    /**
     * @param LetterTemplate $entity
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function update(LetterTemplate $entity)
    {
        $handler = $this->get('ms_letter.letter_template.form.handler');
        if ($handler->process($entity)) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('entity.saved')
            );
            return $this->get('oro_ui.router')->redirectAfterSave(
                [
                    'route'      => 'ms_letter_template_update',
                    'parameters' => ['id' => $entity->getId()],
                ],
                [
                    'route'      => 'ms_letter_template_view',
                    'parameters' => ['id' => $entity->getId()],
                ],
                $entity
            );
        }
        return [
            'entity' => $entity,
            'form'   => $handler->getForm()->createView(),
        ];
    }
}
