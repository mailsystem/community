<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use MailSystem\Bundle\LetterBundle\Entity\Letter;

/**
 * Class LetterController
 * @package MailSystem\Bundle\LetterBundle\Controller
 */
class LetterController extends Controller
{
    /**
     * @Route("/view/{id}", name="ms_letter_view", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="ms_letter_view",
     *      type="entity",
     *      permission="VIEW",
     *      class="MailSystemLetterBundle:Letter"
     * )
     * @param Letter $entity
     * @return array
     */
    public function viewAction(Letter $entity)
    {
        return [
            'entity' => $entity,
        ];
    }

    /**
     * @Route(name="ms_letter_index")
     * @Template
     * @AclAncestor("ms_letter_view")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/update/{id}", name="ms_letter_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="ms_letter_update",
     *      type="entity",
     *      permission="EDIT",
     *      class="MailSystemLetterBundle:Letter"
     * )
     * @param Letter $entity
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Letter $entity)
    {
        return $this->update($entity);
    }

    /**
     * @Route("/create", name="ms_letter_create")
     * @Template("MailSystemLetterBundle:Letter:update.html.twig")
     * @Acl(
     *      id="ms_letter_create",
     *      type="entity",
     *      permission="CREATE",
     *      class="MailSystemLetterBundle:Letter"
     * )
     */
    public function createAction()
    {
        return $this->update(new Letter());
    }

    /**
     * @Route("/delete/{id}", name="ms_letter_delete", requirements={"id"="\d+"})
     * @Acl(
     *      id="ms_letter_delete",
     *      type="entity",
     *      permission="DELETE",
     *      class="MailSystemLetterBundle:Letter"
     * )
     * @param Letter $entity
     * @return JsonResponse
     */
    public function deleteAction(Letter $entity)
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($entity);
        $em->flush();
        return new JsonResponse('', Codes::HTTP_OK);
    }

    /**
     * @param Letter $entity
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function update(Letter $entity)
    {
        $handler = $this->get('ms_letter.letter.form.handler');
        if ($handler->process($entity)) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('entity.saved')
            );
            return $this->get('oro_ui.router')->redirectAfterSave(
                [
                    'route'      => 'ms_letter_update',
                    'parameters' => ['id' => $entity->getId()],
                ],
                [
                    'route'      => 'ms_letter_view',
                    'parameters' => ['id' => $entity->getId()],
                ],
                $entity
            );
        }
        return [
            'entity' => $entity,
            'form'   => $handler->getForm()->createView(),
        ];
    }
}
