<?php
/** {license_text} */

namespace MailSystem\Bundle\LetterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LetterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ms_letter';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'MailSystem\Bundle\LetterBundle\Entity\Letter',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'subject',
            'text',
            [
                'required' => true,
                'label'    => 'ms.letter.subject.label',
            ]
        );
        $builder->add(
            'body',
            'oro_resizeable_rich_text',
            [
                'required' => true,
                'label'    => 'ms.letter.body.label',
            ]
        );
    }
}
