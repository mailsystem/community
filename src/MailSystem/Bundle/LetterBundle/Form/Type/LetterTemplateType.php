<?php
/** {license_text} */

namespace MailSystem\Bundle\LetterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LetterTemplateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ms_letter_template';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'MailSystem\Bundle\LetterBundle\Entity\LetterTemplate',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'text',
            [
                'required' => true,
                'label'    => 'ms.lettertemplate.name.label',
            ]
        );
        $builder->add(
            'body',
            'oro_resizeable_rich_text',
            [
                'required' => true,
                'label'    => 'ms.lettertemplate.body.label',
            ]
        );
    }
}
