<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * Class MailSystemLetterBundle
 * @package MailSystem\Bundle\LetterBundle\Migrations\Schema\v1_0
 */
class MailSystemLetterBundle implements Migration
{
    /**
     * @param Schema   $schema
     * @param QueryBag $queries
     * @return void
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->createTable('ms_letter');
        $table->addColumn('id', 'integer', ['autoincrement' => true]);
        $table->addColumn('organization_id', 'integer', ['notnull' => false]);
        $table->addColumn('business_unit_owner_id', 'integer', ['notnull' => false]);
        $table->addColumn('subject', 'string', ['notnull' => true, 'length' => 255]);
        $table->addColumn('body', 'text', ['notnull' => true]);
        $table->addColumn('created_at', 'datetime', ['precision' => 0, 'notnull' => true]);
        $table->addColumn('updated_at', 'datetime', ['precision' => 0, 'notnull' => false]);
        $table->setPrimaryKey(['id']);

        $table->addForeignKeyConstraint(
            $schema->getTable('oro_organization'),
            ['organization_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null],
            'FK_MS_LETTER_ORGANIZATION'
        );

        $table->addForeignKeyConstraint(
            $schema->getTable('oro_business_unit'),
            ['business_unit_owner_id'],
            ['id'],
            ['onDelete' => 'SET NULL', 'onUpdate' => null],
            'FK_MS_LETTER_BUSINESS_UNIT'
        );
    }
}
