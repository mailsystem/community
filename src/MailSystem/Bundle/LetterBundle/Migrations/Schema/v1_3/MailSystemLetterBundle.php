<?php
/** {license_text} */
namespace MailSystem\Bundle\LetterBundle\Migrations\Schema\v1_3;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;
use Oro\Bundle\CommentBundle\Migration\Extension\CommentExtension;
use Oro\Bundle\CommentBundle\Migration\Extension\CommentExtensionAwareInterface;

/**
 * Class MailSystemLetterBundle
 * @package MailSystem\Bundle\LetterBundle\Migrations\Schema\v1_3
 */
class MailSystemLetterBundle implements Migration, CommentExtensionAwareInterface
{
    /** @var CommentExtension */
    protected $commentExtension;

    /**
     * @param CommentExtension $commentExtension
     */
    public function setCommentExtension(CommentExtension $commentExtension)
    {
        $this->commentExtension = $commentExtension;
    }

    /**
     * @param Schema   $schema
     * @param QueryBag $queries
     * @return void
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $this->commentExtension->addCommentAssociation($schema, 'ms_letter');
    }
}
